## API

```sh
docker build -f docker/api/Dockerfile -t hello-api .
```

### Executar:

```sh
docker run -d --rm --name hello-api -p 8080:8080 hello-api
docker logs -f hello-api
```

### Acessar

```
docker exec -it hello-api bash
```

### Remover

```
docker container rm -f hello-api
```
### Build

```sh
GOOGLE_CONTAINER_REGISTRY_SERVICE_KEY=`cat ../gke-gitlab-11111-8927612a50c7.json`
VERSION=1.0.0
CI_COMMIT_SHA=`git rev-parse HEAD`
gitlab-runner exec docker build-api \
    --docker-privileged \
    --env="CI_COMMIT_SHA=${CI_COMMIT_SHA}" \
    --env="GCP_PROJECT_ID=gke-gitlab-11111" \
    --env="GOOGLE_CONTAINER_REGISTRY_SERVICE_KEY=${GOOGLE_CONTAINER_REGISTRY_SERVICE_KEY}"
```


