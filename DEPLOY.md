### Setup

```
gcloud config set project gke-gitlab-11111
gcloud config set compute/zone us-central1-a
```

### Cluster

Criar

```sh
gcloud container clusters create mdojr-cluster \
    --machine-type=n1-standard-1 \
    --tags=standard \
    --num-nodes=1 \
    --min-nodes=1 \
    --max-nodes=3 \
    --enable-autoscaling \
    --enable-ip-alias \
    --enable-cloud-logging \
    --enable-cloud-monitoring
```

Pegar credenciais

```
gcloud container clusters get-credentials vpc-cluster
```

Remover cluster

```
gcloud container clusters delete vpc-cluster
```

Instalar o Helm

```
gcloud container clusters get-credentials mdojr-cluster
helm init
kubectl create serviceaccount --namespace kube-system tiller
kubectl create clusterrolebinding tiller-cluster-rule --clusterrole=cluster-admin --serviceaccount=kube-system:tiller
kubectl patch deploy --namespace kube-system tiller-deploy -p '{"spec":{"template":{"spec":{"serviceAccount":"tiller"}}}}'
```

### IP

```
gcloud compute addresses list
gcloud compute addresses create mdojr-ingress --global
```

### DNS

Go to https://console.cloud.google.com/net-services/dns/zones/mdojr?project=gke-gitlab-11111  and add names pointing to the address.

- helm-example.mdojr.com.br
- api.helm-example.mdojr.com.br


### TLS

```
gcloud beta compute ssl-certificates create hello-app-cert --domains="helm-example.mdojr.com.br"
gcloud beta compute ssl-certificates create hello-api-cert --domains="api.helm-example.mdojr.com.br"
gcloud beta compute ssl-certificates list
```

### deploy local

```sh
GOOGLE_CONTAINER_REGISTRY_SERVICE_KEY=`cat ../gke-gitlab-11111-8927612a50c7.json`
CI_PROJECT_NAME=${PWD##*/}
CI_COMMIT_SHA=`git rev-parse HEAD`
CI_COMMIT_REF_NAME=`git rev-parse --abbrev-ref HEAD`
VERSION=1.0.0
APP_KEY=tututu
DB_HOST=helloapp
DB_NAME=helloapp
DB_USER=helloapp
DB_PASS=helloapp
gitlab-runner exec docker deploy-api \
    --docker-privileged \
    --env="VERSION=${VERSION}" \
    --env="CI_PROJECT_NAME=${CI_PROJECT_NAME}" \
    --env="APP_KEY=${APP_KEY}" \
    --env="DB_HOST=${DB_HOST}" \
    --env="DB_NAME=${DB_NAME}" \
    --env="DB_USER=${DB_USER}" \
    --env="DB_PASS=${DB_PASS}" \
    --env="CI_COMMIT_SHA=${CI_COMMIT_SHA}" \
    --env="CI_COMMIT_REF_NAME=${CI_COMMIT_REF_NAME}" \
    --env="GCP_PROJECT_ID=gke-gitlab-11111" \
    --env="GCP_COMPUTE_ZONE=us-central1-a" \
    --env="GCP_CLUSTER=mdojr-cluster" \
    --env="GOOGLE_CONTAINER_REGISTRY_SERVICE_KEY=${GOOGLE_CONTAINER_REGISTRY_SERVICE_KEY}"
```

### Migrations

```
kubectl exec -it hello-api-86d5dd9884-zgf5j -c hello-api bash
```