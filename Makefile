c=helloapp-api

env:
	cp .env.example .env
env-test:
	cp .env.testing .env
up:
	docker-compose up
down:
	docker-compose down
jump-in:
	docker exec -it ${c} bash
local-link:
	rm "${PWD}/public/storage" -rf
	ln -s "${PWD}/storage/app/public" "${PWD}/public/storage"
install:
	composer install
migrate-fresh:
	php artisan migrate:fresh
update:
	composer update
test-features:
	php vendor/bin/phpunit --testsuite Features
logs:
	tail -f "storage/logs/lumen-$(shell date +'%Y-%m-%d').log"
secret:
	echo -n ${s} | base64