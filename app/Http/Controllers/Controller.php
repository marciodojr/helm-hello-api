<?php

namespace App\Http\Controllers;

use Laravel\Lumen\Routing\Controller as BaseController;
use App\Http\Helpers\ResponseFormatter;

class Controller extends BaseController
{
    use ResponseFormatter;
}
