<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Entities\Person;
use App\Http\Helpers\ResponseFormatter;
use Illuminate\Http\Response;

class HelloController extends Controller
{
    use ResponseFormatter;

    private $person;

    public function __construct(Person $p)
    {
        $this->person = $p;
    }

    public function index(Request $request)
    {
        $data = $this->validate($request, [
            'amount' => 'integer|min:1',
            'page' => 'integer|min:1'
        ]);

        $amount = min($data['amount'] ?? 10, 100);
        $pageSize = $data['page'] ?? 1;

        $people = $this->person
            ->orderBy('id', 'desc')
            ->limit($amount)
            ->offset(($pageSize - 1) * $amount)
            ->get();

        $total = $this->person->count();

        return $this->successResponse([
            'items' => $people->toArray(),
            'total' => $total
        ]);
    }

    public function store(Request $request)
    {
        $data = $this->validate($request, [
            'firstname' => 'required|min:2|max:255',
        ]);

        $person = $this->person->create($data);
        return $this->successResponse($person->toArray(), Response::HTTP_CREATED);
    }

    public function show(int $id)
    {
        $person = $this->person->find($id);
        return $this->successResponse($person ? $person->toArray() : []);
    }
}
