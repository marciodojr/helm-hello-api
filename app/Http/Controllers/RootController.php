<?php

namespace App\Http\Controllers;

use Illuminate\Http\Response;
use Laravel\Lumen\Application;

class RootController extends Controller
{
    public function __invoke(Application $app) : Response
    {
        return $this->successResponse($app->version());
    }

    public function healthz()
    {
        return $this->successResponse(null, Response::HTTP_OK);
    }
}
