<?php

namespace App\Http\Helpers;

use Illuminate\Http\Response;

trait ResponseFormatter
{
    /**
     * Format success responses
     *
     * @param string|array $data
     * @param int $code
     * @return \Illuminate\Http\Response
     */
    public function successResponse($data = 'ok', int $code = Response::HTTP_OK)
    {
        return response(['data' => $data], $code);
    }

    /**
     * Build error responses
     * @param  string|array $message
     * @param  int $code
     * @return \Illuminate\Http\Response
     */
    public function errorResponse($message, int $code)
    {
        return response(['error' => $message, 'code' => $code], $code);
    }
}
