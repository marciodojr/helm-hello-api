{{/* Gera env para queue e api */}}
{{ define "hello-api.env" -}}
env:
  - name: APP_URL
    valueFrom:
    configMapKeyRef:
      name: {{ .Release.Name }}-config
      key: app.url
  - name: APP_KEY
    valueFrom:
    secretKeyRef:
      name: {{ .Release.Name }}-secrets
      key: app.key
  # Database
  - name: DB_HOST
    valueFrom:
    secretKeyRef:
      name: {{ .Release.Name }}-secrets
      key: db.host
  - name: DB_PORT
    valueFrom:
    secretKeyRef:
      name: {{ .Release.Name }}-secrets
      key: db.port
  - name: DB_DATABASE
    valueFrom:
    secretKeyRef:
      name: {{ .Release.Name }}-secrets
      key: db.name
  - name: DB_USERNAME
    valueFrom:
    secretKeyRef:
      name: {{ .Release.Name }}-secrets
      key: db.user
  - name: DB_PASSWORD
    valueFrom:
    secretKeyRef:
      name: {{ .Release.Name }}-secrets
      key: db.pass
{{ end }}
