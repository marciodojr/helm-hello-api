<?php

namespace AppTests\Features;

use Illuminate\Http\Response;
use AppTests\TestCase;

class ApiRootTest extends TestCase
{
    /**
     * Check "/" endpoint
     *
     * @return void
     */
    public function testRootSuccess()
    {
        $this->get('/');
        $this->assertResponseStatus(Response::HTTP_OK);
    }

    /**
     * Check "/" endpoint
     *
     * @return void
     */
    public function testHealthzSuccess()
    {
        $this->get('/healthz');
        $this->assertResponseStatus(Response::HTTP_OK);
    }
}
